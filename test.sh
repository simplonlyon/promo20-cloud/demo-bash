#!/usr/bin/env bash

CURRENT_USER=$(whoami)


##################################################################
#
#		Récupération du shell de l'utilisateur
#
##################################################################


CURRENT_SHELL=$(grep ^$CURRENT_USER /etc/passwd | awk -F":" '{print $NF}')

echo "Le script est lancé en tant que $CURRENT_USER avec le shell $CURRENT_SHELL"

##################################################################
#
# 	Si l'utilisateur est root, afficher "Vous êtes administrateur"
#	Sinon, afficher "Vous n'êtes pas administrateur"
#
##################################################################

if [ $CURRENT_USER != "root" ]
then
	echo "Vous n'êtes pas administrateur"
else
        echo "Vous êtes administrateur"
fi


##################################################################
#
#	Afficher la taille de chacun des fichiers du dossier courant
#	Les dossiers sont ignorés
#
##################################################################


for FILE in *; do
	if [ -f "$FILE" ]; then
		# Faire la somme des tailles
		TAILLE_COURANTE=$(stat -c%s "$FILE")
		TAILLE_T=$((${TAILLE_T} + ${TAILLE_COURANTE}))
		# Afficher le fichier courant
		echo "Le fichier $FILE pèse $TAILLE_COURANTE octets"
	fi
done
# Afficher la taille totale
echo "Le poids total est $TAILLE_T"
